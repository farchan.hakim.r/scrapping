from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client
from collections import OrderedDict
db = GraphDatabase("http://localhost:7474", username="neo4j", password="123")
import datetime


import sys
sys.path.append("")
from CountriesScrapp import *

def insertGraphCountry(list):
    Countries = db.labels.create("Countries")
    Regions = db.labels.create("Regions")

    data_country = []
    data_region = []

    uniqueRegion = []
    for i in range(0, len(list)):
        print(list[i][1])
        data= db.nodes.create(name_country=list[i][1], href=list[i][2])
        data_country.append([data, list[i][0]])
        Countries.add(data)
        uniqueRegion.append(list[i][0])

    resultRegion = OrderedDict( (x,1) for x in uniqueRegion ).keys()
    for region in resultRegion:
        print(region)
        if(region != 'Region'):
            data= db.nodes.create(name_region=region)
            data_region.append([data, region])
            Regions.add(data)

    return data_country, data_region

def relationshipGraphCountry(countries, regions):

    for country in countries:
        for region in regions:
            if(region[1] == country[1]):
                country[0].relationships.create("region in", region[0])
                print(country[0])

# def insertGraphMountain(list):

def getListMountainByCountry(countries):

    Mountains = db.labels.create("Mountain")

    for country in countries:
      
        d_country = "%s" % country[0]["name_country"]
        d_href = "%s" % country[0]["href"]

        print(country)
        if d_href != 'Link':
            mountains = listMountatin(d_href)
           
            for mountain in mountains:
                print(mountain)
                data= db.nodes.create(name_mountain=mountain[0], height=mountain[1], href=mountain[2])
                Mountains.add(data)
                data.relationships.create("country in", country[0])
            # break


def getListWheaterMountain(mountains):

    Wheaters = db.labels.create("Weather")

    for mountain in mountains:
      
        d_mountain = "%s" % mountain[0]["name_mountain"]
        d_href = "%s" % mountain[0]["href"]

        print(d_mountain)
        
        if d_href != 'Link' :
            data, summary = listWheater(d_href)
            datetime_object = datetime.datetime.now()
            if(data != False):
                 
                data_object= db.nodes.create(
                    summary=summary, 
                    wind=(data[0]+data[1]+data[2])/3,
                    summary_wind = data[3] + data[4] + data[5],
                    rain = (data[6]+data[7]+data[8])/3,
                    snow = (data[9]+data[10]+data[11])/3,
                    max_temp = (data[12]+data[13]+data[14])/3,
                    min_temp = (data[15]+data[16]+data[17])/3,
                    chill = (data[18]+data[19]+data[20])/3,
                    freezing_level = (data[21]+data[22]+data[23])/3,
                    sunrise = data[24],
                    sunset = data[25],
                    datetime = datetime_object
                    
                )
                Wheaters.add(data_object)
                data_object.relationships.create("relate in", mountain[0])
                
            # break

def selectCountry():
    q = 'MATCH (u:Countries)  RETURN u'
    results = db.query(q, returns=(client.Node, str, client.Node))
    for r in results:
        # print(r)
        print("(%s), (%s)" % (r[0]["name_country"], r[0]["href"]))
    return results

def selectMountain():
    q = 'MATCH (u:Mountain)  RETURN u'
    results = db.query(q, returns=(client.Node, str, client.Node))
    print(results)
    # for r in results:
    #     # print(r)
    #     print("(%s), (%s)" % (r[0]["name_mountain"], r[0]["href"]))
    return results

if __name__== "__main__":
    country = getListCountries('https://www.mountain-forecast.com/countries')
    
    #FOR LOAD COUNTRY AND REGION 
    # list_country, list_region = insertGraphCountry(country)
    # relationshipGraphCountry(list_country, list_region)

    # #FOR LOAD MOUNTAIN 
    # result_country = selectCountry()
    # getListMountainByCountry(result_country)

    #FOR LOAD WHEATER IN MOUNTAIN (RUN PROGRAM WITH INTERVAL TIME)
    result_mountains = selectMountain()
    getListWheaterMountain(result_mountains)