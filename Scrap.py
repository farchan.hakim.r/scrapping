import requests
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd 
import urllib.request  as urllib2 
import csv 

def getListCountries(link):
    req = requests.get(link)
    soup = BeautifulSoup(req.text, "lxml")
    listregion = soup.find_all('h2')
    res = []
    for b in listregion:
        res.append(b)

    tabel=np.array([["Region", "Country", "Link"]])


    for i in range(0, len(res)):
        if(res[i].text==""):
            listcountry = res[i].next_element.next_element.find_all('a', href=True)
        else:
            listcountry = res[i].next_element.next_element.next_element.find_all('a', href=True)
        for a in listcountry:
            tabel = np.append(tabel, [[res[i].text, a.text, "https://www.mountain-forecast.com" + a.get('href')]], axis = 0)
    return tabel

def listMountatin(link):
    opener = urllib2.build_opener()
  
    opener.addheaders = [{'User-agent', 'Mozilla/5.0'}]
    URL = link + "?top100=yes"
   
    ourUrl = opener.open(URL).read()
    
    soup = BeautifulSoup(ourUrl)
    tab = soup.find_all("ul", {"class": "b-list-table"})

    tabel = []
    # title = soup.title.text
    for data in tab:
        for summary in data.find_all("li", {"class": "b-list-table__item"}):
            for href in summary.find_all('a', href=True):
                mountain  = href['href'].split('/')
                tabel.append([mountain[2],mountain[4], "https://www.mountain-forecast.com" + href['href']])
                # print([mountain[2], link[1], link[0], mountain[4],href['href']])
              
    return tabel
                
def listWheater(link):
    opener = urllib2.build_opener()
    opener.addheaders = [{'User-agent', 'Mozilla/5.0'}]
    try:
         URL =  link
         ourUrl = opener.open(URL).read()
    except urllib2.HTTPError:
        print('NOT FOUND')
        return False, False

    
    soup = BeautifulSoup(ourUrl)
    title = soup.title.text
    tab = soup.find_all("table", {"class": "forecast__table forecast__table--js"})


    summary = []
    # wind = []
    # summary_wind = []
    # rain = []
    # snow = []
    # max_temp = []
    # min_temp = []
    # chill = []
    # freezing_level = []
    # sunrise = []
    # sunset = []
    value_total = []
    
    for data in tab:
        for summary_val_1 in data.find_all("tr", {"class": "forecast__table-description show-for-large days-summaries"}):
            for summary_val_2 in summary_val_1.find_all("span", {"class": "phrase"}):
                summary.append(summary_val_2.text)

        for wind_val_1 in data.find_all("tr", {"class": "forecast__table-wind"}):
            count_wind = 0
            for wind_val_2 in (wind_val_1.find_all("span")) :
                if wind_val_2.get_text().isdigit() and (count_wind < 3) :
                    value_total.append(float(wind_val_2.get_text()))
                    count_wind = count_wind + 1

        for summary_wind_1 in (data.find_all("tr", {"class": "forecast__table-summary"})):
            count_summary_wind = 0
            for summary_wind_2 in summary_wind_1.find_all("td"):
                if  (count_summary_wind < 3) :
                    value_total.append(summary_wind_2.get_text())
                    count_summary_wind = count_summary_wind + 1

        for rain_1 in (data.find_all("tr", {"class": "forecast__table-rain"})):
            count_rain = 0
            for rain_2 in (rain_1.find_all("span", {"class": "rain"})):
                if (count_rain < 3):
                    if(rain_2.get_text() == '-'):    
                        value_total.append(float('0'))
                        count_rain = count_rain + 1
                    else:
                        value_total.append(float(rain_2.get_text()))
                        count_rain = count_rain + 1

        for snow_1 in (data.find_all("tr", {"class": "forecast__table-snow"})):
            count_snow = 0
            for snow_2 in (snow_1.find_all("span", {"class" : "snow"})):
                if (count_snow < 3):
                    if(snow_2.get_text() == '-'):
                        value_total.append(float('0'))
                        count_snow = count_snow + 1
                    else:
                        value_total.append(float(snow_2.get_text()))
                        count_snow = count_snow + 1

        for max_temp_1 in (data.find_all("tr", {"class": "forecast__table-max-temperature"})):
            count_max_temp = 0
            for max_temp_2 in (max_temp_1.find_all("span", {"class" : "temp"})):
                if (count_max_temp < 3):
                    if(max_temp_2.get_text() == '-'):
                        value_total.append(float('0'))
                        count_max_temp = count_max_temp + 1
                    else:
                        value_total.append(float(max_temp_2.get_text()))
                        count_max_temp = count_max_temp + 1

        for min_temp_1 in (data.find_all("tr", {"class": "forecast__table-min-temperature"})):
            count_min_temp = 0
            for min_temp_2 in (min_temp_1.find_all("span", {"class" : "temp"})):
                if (count_min_temp < 3):
                    if(min_temp_2.get_text() == '-'):
                        value_total.append(float('0'))
                        count_min_temp = count_min_temp + 1
                    else:
                        value_total.append(float(min_temp_2.get_text()))
                        count_min_temp = count_min_temp + 1
                    
        
        for chill_1 in (data.find_all("tr", {"class": "forecast__table-chill"})):
            count_chill = 0
            for chill_2 in (chill_1.find_all("span", {"class" : "temp"})):
                if (count_chill < 3):
                    if(chill_2.get_text() == '-'):
                        value_total.append(float('0'))
                        count_chill = count_chill + 1
                    else:
                        value_total.append(float(chill_2.get_text()))
                        count_chill = count_chill + 1
                        

        for freezing_1 in (data.find_all("tr", {"class": "forecast__table-freezing-level"})):
            count_freezing = 0
            for freezing_2 in (freezing_1.find_all("span", {"class" : "heightfl"})):
                if (count_freezing < 3):
                   
                    if(freezing_2.get_text() == '-'):
                        value_total.append(float('0'))
                        count_freezing = count_freezing + 1
                    else:
                        value_total.append(float(freezing_2.get_text()))
                        count_freezing = count_freezing + 1


        for sunrise_1 in (data.find_all("tr", {"class": "forecast__table-sunrise"})):
            count_sunrise = 0
            for sunrise_2 in (sunrise_1.find_all("span", {"class" : "forecast__table-value"})):
                if (count_sunrise == 0):
                    value_total.append(sunrise_2.get_text())
                    count_sunrise = count_sunrise + 1
                        
                        

        for sunset_1 in (data.find_all("tr", {"class": "forecast__table-sunset"})):
            count_sunset = 0
            for sunset_2 in (sunset_1.find_all("span", {"class" : "forecast__table-value"})):
                if (count_sunset == 1):
                        value_total.append(sunset_2.get_text())
                count_sunset = count_sunset + 1

    return value_total, summary


   